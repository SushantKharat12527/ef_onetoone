﻿using Sol_EF_One_to_One.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_One_to_One
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<UserEntity> userEntityObj = await new UserDal().GetUserData();

            }).Wait();
        }
    }
}
