﻿using Sol_EF_One_to_One.EF;
using Sol_EF_One_to_One.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_One_to_One
{
    public class UserDal
    {
        #region Declaration
        private UserdbEntities db = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            db = new UserdbEntities();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<UserEntity>> GetUserData()
        {
            return await Task.Run(() =>
            {

                var getQuery =
                    db
                    ?.tblUsers
                    ?.AsEnumerable()
                    ?.Select(
                        (letblUserObj) => new UserEntity()
                        {

                            UserId = letblUserObj?.UserId ?? 0,
                            FirstName = letblUserObj?.FirstName ?? null,
                            LastName = letblUserObj?.LastName ?? null,
                            UserLogin = new UserLoginEntity()
                            {
                                UserName = letblUserObj?.tblUserLogin?.UserName ?? null,
                                Password = letblUserObj?.tblUserLogin?.Password ?? null
                            },
                            UserCommunication = new UserCommunication()
                            {
                                EmailId = letblUserObj?.tblUsersCommunication?.EmailId ?? null,
                                MobileNo = letblUserObj?.tblUsersCommunication?.MobileNo ?? null
                            }
                        }

                        )
                        ?.ToList();

                return getQuery;
            });
        }
        #endregion 
    }
}

